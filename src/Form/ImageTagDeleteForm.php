<?php

/**
 * @file
 * Contains \Drupal\image_tag\Entity\Form\ImageTagDeleteForm.
 */

namespace Drupal\image_tag\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a form for deleting a image_tag entity.
 *
 * @ingroup image_tag
 */
class ImageTagDeleteForm extends ContentEntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete %label?', array('%label' => $this->entity->label()));
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelURL() {
    return new Url('image_tag.list');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    $entity->delete();

    $this->logger('image_tag')->notice('@type: deleted %label.',
      array(
        '@type' => $this->entity->bundle(),
        '%label' => $this->entity->label(),
      ));

    $type_storage = $this->entityManager->getStorage('image_tag_type');
    $type = $type_storage->load($this->entity->bundle())->label();
    drupal_set_message(t('@type %label has been deleted.', array('@type' => $type, '%label' => $this->entity->label())));

    $form_state->setRedirect('image_tag.list');
  }

}
