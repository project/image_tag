<?php

/**
 * @file
 * Contains \Drupal\image_tag\ImageTagTypeForm.
 */

namespace Drupal\image_tag\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for editing an image tag type entity.
 */
class ImageTagTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $type = $this->entity;

    // Label.
    $form['label'] = array(
      '#type' => 'textfield',
      '#title' => t('Label'),
      '#maxlength' => 255,
      '#default_value' => $type->label(),
      '#description' => t("Provide a label for this image tag type."),
      '#required' => TRUE,
    );

    // ID.
    $form['id'] = array(
      '#type' => 'machine_name',
      '#disabled' => !$type->isNew(),
      '#default_value' => $type->id(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#machine_name' => array(
        'exists' => '\Drupal\image_tag\Entity\ImageTagType::load',
      ),
    );

    // Description.
    $form['description'] = array(
      '#type' => 'textarea',
      '#default_value' => $type->description,
      '#description' => t('Enter a description for this image tag type.'),
      '#title' => t('Description'),
    );

    // Actions.
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $type = $this->entity;
    $type->id = trim($type->id());
    $type->label = trim($type->label);

    // Save.
    $status = $type->save();

    // Set message and log.
    $t_args = array('%label' => $type->label());
    if ($status == SAVED_UPDATED) {
      drupal_set_message(t('Image tag type %label has been updated.', $t_args));
      $this->logger('image_tag')->notice('Image tag type %label has been updated.', $t_args);
    }
    elseif ($status == SAVED_NEW) {
      drupal_set_message(t('Image tag type %label has been added.', $t_args));
      $this->logger('image_tag')->notice('Image tag type %label has been added.', $t_args);
    }

    $form_state->setRedirect('image_tag.type_admin');
  }

}
