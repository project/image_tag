<?php
/**
 * @file
 * Contains \Drupal\image_tag\Entity\ImageTag.
 */

namespace Drupal\image_tag\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\image_tag\ImageTagInterface;
use Drupal\user\UserInterface;

/**
 * Defines the ImageTag entity.
 *
 * @ingroup image_tag
 *
 * @ContentEntityType(
 *   id = "image_tag",
 *   label = @Translation("Image tag"),
 *   base_table = "image_tag",
 *   bundle_entity_type = "image_tag_type",
 *   fieldable = TRUE,
 *   field_ui_base_route = "entity.image_tag_type.edit_form",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "canonical" = "entity.image_tag.canonical",
 *     "edit-form" = "entity.image_tag.edit_form",
 *     "delete-form" = "entity.image_tag.delete_form",
 *   },
 *   handlers = {
 *     "access" = "Drupal\image_tag\ImageTagAccessControlHandler",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "form" = {
 *       "default" = "Drupal\Core\Entity\ContentEntityForm",
 *       "add" = "Drupal\Core\Entity\ContentEntityForm",
 *       "edit" = "Drupal\Core\Entity\ContentEntityForm",
 *       "delete" = "Drupal\image_tag\Form\ImageTagDeleteForm",
 *     },
 *   },
 * )
 */
class ImageTag extends ContentEntityBase implements ImageTagInterface {

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    // Set tag owner to current user.
    $values += array(
      'uid' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->bundle();
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    // @todo: Better label, maybe a token pattern.
    return t('Image tag: !id', array('!id' => $this->id()));
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedTime() {
    return $this->get('changed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setChangedTime($timestamp) {
    $this->set('changed', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    // Standard field, used as unique if primary index.
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Image tag ID'))
      ->setDescription(t('The image tag ID.'))
      ->setSetting('unsigned', TRUE)
      ->setReadOnly(TRUE);

    // Standard field, unique outside of the scope of the current project.
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The image tag UUID.'))
      ->setReadOnly(TRUE);

    // Bundle type.
    $fields['type'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Image tag type'))
      ->setDescription(t('The image tag type.'))
      ->setSetting('target_type', 'image_tag_type');

    // User ID of image tag author.
    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Tagged by'))
      ->setDescription(t('The user who created the image tag.'))
      ->setTranslatable(TRUE)
      ->setSetting('target_type', 'user')
      ->setDefaultValue(0);

    // Created.
    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the image tag was created.'));

    // Changed.
    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the image tag was last edited.'));

    // Reference to containing image file entity.
    // A file or image basefield adds too many db columns, we just want the simple reference.
    $fields['image'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Image'))
      ->setDescription(t('The image containing the image tag.'))
      ->setSetting('target_type', 'file')
      ->setSetting('handler', 'default');

    // Box Top.
    $fields['box_top'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Box top'))
      ->setDescription(t('The top coordinate of the image tag in pixels.'))
      ->setSetting('unsigned', TRUE)
      ->setDefaultValue(0);

    // Box Left.
    $fields['box_left'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Box left'))
      ->setDescription(t('The left coordinate of the image tag in pixels.'))
      ->setSetting('unsigned', TRUE)
      ->setDefaultValue(0);

    // Box Height.
    $fields['box_height'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Box height'))
      ->setDescription(t('The height of the image tag in pixels.'))
      ->setSetting('unsigned', TRUE)
      ->setDefaultValue(0);

    // Box Width.
    $fields['box_width'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Box width'))
      ->setDescription(t('The width of the image tag in pixels.'))
      ->setSetting('unsigned', TRUE)
      ->setDefaultValue(0);

    return $fields;
  }
}
