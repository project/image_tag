<?php

/**
 * @file
 * Contains \Drupal\image_tag\ImageTagPermissions.
 */

namespace Drupal\image_tag;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\image_tag\Entity\ImageTagType;

/**
 * Defines a class containing permission callbacks.
 */
class ImageTagPermissions {

  use StringTranslationTrait;

  /**
   * Returns an array of image tag type permissions.
   *
   * @return array
   */
  public function typePermissions() {
    $perms = array();
    foreach (ImageTagType::loadMultiple() as $type) {
      $perms += $this->buildPermissions($type);
    }

    return $perms;
  }

  /**
   * Builds a standard list of permissions for a given image tag type.
   *
   * @param \Drupal\image_tag\Entity\ImageTagType $type
   *   The machine name of the image tag type.
   *
   * @return array
   *   An array of permission names and descriptions.
   */
  protected function buildPermissions(ImageTagType $type) {
    $type_id = $type->id();
    $type_params = array('%type_name' => $type->label());

    return array(
      "view $type_id image tags" => array(
        'title' => $this->t('%type_name: View image tags', $type_params),
      ),
      "create $type_id image tags" => array(
        'title' => $this->t('%type_name: Create new image tags', $type_params),
      ),
      "edit own $type_id image tags" => array(
        'title' => $this->t('%type_name: Edit own image tags', $type_params),
      ),
      "edit any $type_id image tags" => array(
        'title' => $this->t('%type_name: Edit any image tags', $type_params),
      ),
      "delete own $type_id image tags" => array(
        'title' => $this->t('%type_name: Delete own image tags', $type_params),
      ),
      "delete any $type_id image tags" => array(
        'title' => $this->t('%type_name: Delete any image tags', $type_params),
      ),
    );
  }

}
