<?php
/**
 * @file
 * Contains \Drupal\image_tag\ImageTagInterface.
 */

namespace Drupal\image_tag;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an Image tag entity.
 * @ingroup image_tag
 */
interface ImageTagInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Returns the image tag type.
   *
   * @return string
   *   The image tag type.
   */
  public function getType();

  /**
   * Returns the image tag creation timestamp.
   *
   * @return int
   *   Creation timestamp of the image tag.
   */
  public function getCreatedTime();

  /**
   * Sets the image tag creation timestamp.
   *
   * @param int $timestamp
   *   The image tag creation timestamp.
   *
   * @return \Drupal\image_tag\ImageTagInterface
   *   The called image tag entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the image tag changed timestamp.
   *
   * @return int
   *   Changed timestamp of the image tag.
   */
  public function getChangedTime();

  /**
   * Sets the image tag changed timestamp.
   *
   * @param int $timestamp
   *   The image tag changed timestamp.
   *
   * @return \Drupal\image_tag\ImageTagInterface
   *   The called image tag entity.
   */
  public function setChangedTime($timestamp);

}