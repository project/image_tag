<?php

/**
 * @file
 * Contains \Drupal\image_tag\Controller\ImageTagController
 */

namespace Drupal\image_tag\Controller;

use Drupal\image_tag\ImageTagTypeInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ImageTagController extends ControllerBase {

  /**
   * The image tag storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $imageTagStorage;

  /**
   * The image tag type storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $imageTagTypeStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $entity_manager = $container->get('entity.manager');
    return new static(
      $entity_manager->getStorage('image_tag'),
      $entity_manager->getStorage('image_tag_type')
    );
  }

  /**
   * Constructs a ImageTag object.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $image_tag_storage
   *   The image tag storage.
   * @param \Drupal\Core\Entity\EntityStorageInterface $image_tag_type_storage
   *   The image tag type storage.
   */
  public function __construct(EntityStorageInterface $image_tag_storage, EntityStorageInterface $image_tag_type_storage) {
    $this->imageTagStorage = $image_tag_storage;
    $this->imageTagTypeStorage = $image_tag_type_storage;
  }

  /**
   * Displays add image tag links for available types.
   *
   * @return array
   *   A render array for a list of the image tag types that can be added or
   *   if there is only one image tag type defined for the site, the function
   *   redirects to the image tag add page for that image tag type.
   */
  public function addPage() {
    $content = array();

    // Only use types the user has access to.
    foreach ($this->entityManager()->getStorage('image_tag_type')->loadMultiple() as $type) {
      if ($this->entityManager()->getAccessControlHandler('image_tag')->createAccess($type->id)) {
        $content[$type->id] = $type;
      }
    }

    // Bypass the node/add listing if only one content type is available.
    if (count($content) == 1) {
      $type = array_shift($content);
      return $this->redirect('image_tag.add_form', array('image_tag_type' => $type->id()));
    }

    return array(
      '#theme' => 'image_tag_add_list',
      '#content' => $content,
    );
  }

  /**
   * Presents the image tag creation form.
   *
   * @param \Drupal\image_tag\ImageTagTypeInterface $image_tag_type
   *   The image tag type to add.
   *
   * @return array
   *   A form array as expected by drupal_render().
   */
  public function addForm(ImageTagTypeInterface $image_tag_type) {
    $image_tag = $this->imageTagStorage->create(array(
      'type' => $image_tag_type->id(),
    ));

    return $this->entityFormBuilder()->getForm($image_tag);
  }

  /**
   * Provides the page title for this controller.
   *
   * @param \Drupal\image_tag\ImageTagTypeInterface $image_tag_type
   *   The image tag type being added.
   *
   * @return string
   *   The page title.
   */
  public function getAddFormTitle(ImageTagTypeInterface $image_tag_type) {
    return $this->t('Add %type image tag', array('%type' => $image_tag_type->label()));
  }

}
