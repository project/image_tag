<?php

/**
 * @file
 * Contains \Drupal\image_tag\Entity\ImageTagType.
 */

namespace Drupal\image_tag\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\image_tag\ImageTagTypeInterface;

/**
 * Defines the image tag type entity.
 *
 * @ConfigEntityType(
 *   id = "image_tag_type",
 *   label = @Translation("Image tag type"),
 *   bundle_of = "image_tag",
 *   config_prefix = "type",
 *   admin_permission = "administer image tag",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   links = {
 *     "delete-form" = "entity.image_tag_type.delete_form",
 *     "edit-form" = "entity.image_tag_type.edit_form",
 *   },
 *   handlers = {
 *     "list_builder" = "\Drupal\image_tag\ImageTagTypeListBuilder",
 *     "form" = {
 *       "default" = "Drupal\image_tag\Form\ImageTagTypeForm",
 *       "add" = "Drupal\image_tag\Form\ImageTagTypeForm",
 *       "edit" = "Drupal\image_tag\Form\ImageTagTypeForm",
 *       "delete" = "Drupal\image_tag\Form\ImageTagTypeDeleteForm",
 *     },
 *   },
 * )
 */
class ImageTagType extends ConfigEntityBundleBase implements ImageTagTypeInterface {

  /**
   * The custom image tag type ID.
   *
   * @var string
   */
  public $id;

  /**
   * The custom image tag type label.
   *
   * @var string
   */
  public $label;

  /**
   * The description of the image tag type.
   *
   * @var string
   */
  public $description;
}
