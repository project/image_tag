<?php

/**
 * @file
 * Contains \Drupal\image_tag\ImageTagAccessControlHandler
 */

namespace Drupal\image_tag;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the image tag entity.
 *
 * @see \Drupal\image_tag\Entity\ImageTag.
 */
class ImageTagAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    $perm = "create $entity_bundle image tags";
    return AccessResult::allowedIfHasPermission($account, $perm);
  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, $langcode, AccountInterface $account) {
    $type = $entity->bundle();
    $op = $operation === 'update' ? 'edit' : $operation;

    // Check if account can perform operation on any.
    $perm = "$op any $type image tags";
    if ($account->hasPermission($perm, $account)) {
      return AccessResult::allowed()->cachePerRole();
    }
    // Check if account can perform operation on own.
    else if ($account->id() == $entity->getOwnerId()) {
      $perm = "$op any $type image tags";
      if ($account->hasPermission($perm, $account)) {
        return AccessResult::allowed()->cachePerRole();
      }
    }

    // Defer to parent class.
    return parent::checkAccess($entity, $operation, $langcode, $account);
  }

}
