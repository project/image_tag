<?php

/**
 * @file
 * Contains \Drupal\image_tag\Form\ImageTagTypeDeleteForm.
 */

namespace Drupal\image_tag\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a confirmation form for deleting an image tag type entity.
 */
class ImageTagTypeDeleteForm extends EntityConfirmFormBase {

  /**
   * The query factory to create entity queries.
   *
   * @var \Drupal\Core\Entity\Query\QueryFactory
   */
  public $queryFactory;

  /**
   * Constructs a new ImageTagTypeDeleteForm object.
   *
   * @param \Drupal\Core\Entity\Query\QueryFactory $query_factory
   *   The entity query object.
   */
  public function __construct(QueryFactory $query_factory) {
    $this->queryFactory = $query_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.query')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the %label image tag type?', array('%label' => $this->entity->label()));
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('image_tag.type_admin');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // @todo: We can probably do something more helpful here.
    $image_tags = $this->queryFactory->get('image_tag')->condition('type', $this->entity->id())->execute();
    if (!empty($image_tags)) {
      $t_args = array('%label' => $this->entity->label());
      $caption = '<p>' . format_plural(count($image_tags), 'There is 1 %label image tag on your site. You can not remove this type until you have removed all of the %label image tags.', 'There are @count %label image tags on your site. You may not remove this type until you have removed all of the %label image tags.', $t_args) . '</p>';
      $form['description'] = array('#markup' => $caption);
      return $form;
    }
    else {
      return parent::buildForm($form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->entity->delete();
    $t_args = array('%label' => $this->entity->label());
    drupal_set_message(t('The %label image tag type has been deleted.', $t_args));
    $this->logger('image_tag')->notice('Image tag type %label has been deleted.', $t_args);
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
