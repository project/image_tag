<?php

/**
 * @file
 * Provides page callbacks for image_tag.
 */

use Drupal\Component\Utility\Xss;
use Drupal\Core\Url;

/**
 * Prepares variables for a image tag type creation list templates.
 *
 * Default template: image-tag-add-list.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - content: An array of image tag types.
 *
 * @see image_tag_add_page()
 */
function template_preprocess_image_tag_add_list(&$vars) {
  $vars['types'] = array();
  if (!empty($vars['content'])) {
    foreach ($vars['content'] as $type) {
      $vars['types'][$type->id()] = array(
        'type' => $type->id(),
        'title' => $type->label(),
        'add_link' => \Drupal::l($type->label(), new Url('image_tag.add_form', array('image_tag_type' => $type->id()))),
        'description' => Xss::filterAdmin($type->description),
      );
    }
  }
}
