<?php

/**
 * @file
 * Contains \Drupal\image_tag\Entity\ImageTagTypeInterface.
 */

namespace Drupal\image_tag;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a image tag type entity.
 */
interface ImageTagTypeInterface extends ConfigEntityInterface {

}
